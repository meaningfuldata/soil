const { isAbsolute, dirname, relative, resolve } = require('path')

/**
 * Resolve `<root>/src/components/Component.ts` to `<root>/tests/unit/snapshot/components/Component.spec.snap.js`
 */
module.exports = {
  // NOTE: `snapshotExtension` is ".snap"
  resolveSnapshotPath(testPath, snapshotExtension) {
    const relativePath = isAbsolute(testPath)
      ? relative(resolve(testPath, __dirname, '../../src'), testPath)
      : testPath

    return `tests/unit/snapshots/${relativePath}`.replace(
      /\.ts$/,
      `${snapshotExtension}.js`
    )
  },
  resolveTestPath(snapshotFilePath, snapshotExtension) {
    const rootPath = resolve(snapshotFilePath, __dirname, '../..')
    const regexp = new RegExp(`${snapshotExtension}.js$`)

    return relative(
      `${rootPath}/tests/unit/snapshots`,
      snapshotFilePath
    ).replace(regexp, '.ts')
  },
  testPathForConsistencyCheck: 'some/example.spec.ts'
}
