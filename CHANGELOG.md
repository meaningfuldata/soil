# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Fixed

### Removed

## [0.7.13]

### Added

### Changed

- Refactor `SMenuBar`.
- Emits events and watch collapsed changes on `SList`

### Fixed

- Close dropdown whe open another from same menu.


## [0.7.12] - 2023-01-09

### Added

- Allow nested dropdowns.

### Fixed

- Display correctly filters in `STable`

## [0.7.11] - 2022-10-28

### Added

- Allow clicking on the title of `SList` to collapse or expand all the items.

### Changed

- Change cursor of `SDropdownMenu`.

## [0.7.10]

...

## [0.7.9]

...

## [0.7.8]

...

## [0.7.7] - 2022-09-12

### Added

- Create the `SCollapsible` component.
- Allow using `STable` without pagination.
- Change the style of the disabled items of `SDropdownMenu`.

### Changed

- Use the `SCollapsible` component in `SList` instead of `OCollapse`.
- Rename events of `SList` and `SListExplorer`.

### Fixed

- Display the search icon in the column headers of `STable`.

### Removed

- Remove the dropdown menus of `SList` and `SListExplorer`.

## [0.7.6] - 2022-08-22

### Added

- Expose the `downloadImage` and `downloadJSON` utilities

## [0.7.5] - 2022-08-09

### Added

- Create the `SContextMenu` component.
- Create the `SList` component.
- Create the `SListExplorer` component.

### Changed

- Upgrade to Vite 3.
- Rename the `SExplorer` to `STreeExplorer`.

## [0.7.4] - 2022-07-01

### Added

- Install and configure ESLint.
- Add scripts `lint:check` and `lint:fix` to lint and fix code.
- Add scripts `style:check` and `style:fix` to check and fix code style.

### Changed

- Apply ESLint suggestions.
- Apply Prettier suggestions.

## [0.7.3] - 2022-06-27

...

## [0.6.5] - 2021-12-21

### Added

- Create the `STextIcon` component.
- Create the `SExplorer` component.
- Create the `SSearchField` component.
- Add default and subbar slots for the `SSidebar` component.

### Changed

- Improvements to the `menu-input` event of the `SDropdownMenu` component.
- Improvements to the `SIcon` component.

## [0.6.3] - 2021-12-13

### Changed

- Upgrade Vue to `3.2.26`
- Upgrade Vite to `2.7.1`

## [0.6.2] - 2021-12-01

### Changed

- Rename slot `logo` of `SNavbar` to `home`.
- Upgrade Vue to `3.2.23`

### Fixed

- `SNavbar` does not render empty elements if their inner slots are not defined.

### Removed

- The `middle` slot of `SNavbar` does not include a link to the home.

## [0.6.1] - 2021-11-30

### Added

- Include metadata on `package.json`.

### Fixed

- Fix name of setting `COMPONENTS.SIcon.defaultIconPack`.

## [0.6.0] - 2021-11-29

**First release**
