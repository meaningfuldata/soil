module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    '@vue/typescript/recommended',
    'eslint:recommended',
    'prettier'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
  },
  overrides: [
    {
      files: [
        '.build/**/*.spec.{j,t}s',
        'src/**/*.spec.{j,t}s',
        'tests/unit/**/*.spec.{j,t}s'
      ],
      env: {
        jest: true
      }
    }
  ]
}
