# Soil

WIP: this project is still at an early stage of development.

## Features
 - [Vue 3](https://vuejs.org/) + [Vue Router](https://router.vuejs.org/)
 - Node 16
 - [TypeScript 4.1](https://www.typescriptlang.org/)
 - [SASS](https://sass-lang.com/) with CSS modules
 - [Oruga](https://oruga.io/)
 - [Bulma](https://bulma.io/)
 - Unit tests with Jest + Chai
 - End to end tests with [Cypress](https://www.cypress.io/)
 - [Storybook](https://storybook.js.org/)
 - Yarn
 - Babel
 - Linters for Vue ([recommended rules](https://vuejs.org/v2/style-guide/)), TypeScript and JavaScript
 - Code formatting via Prettier
 - Font Awesome 5 solid icons
 - Commit message linting with [commitlint](https://commitlint.js.org/), using an improved version of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
 - [Internationalization](https://kazupon.github.io/vue-i18n/)

## Conventions
### Folder per component domain
All the related code is aggregated:
 - Components
 - External services
 - Types
 - Constants
 - Unit tests
 - Storybook stories
 - Fixtures (used on unit tests and Storybook stories)
 - Mocks

Components are defined and used with their entire namespace (`SComponent`), although they are stored without it (`domain/SComponent`).

### Folder per layout
Similar to components, layouts should include all the children components that exist only to support them.
Only the layout component should be exported.
That main component could be named `_.vue` to keep the folder as the name (in `layouts/MyLayout/_.vue` the name is `LayoutMyName`).

### CSS modules
Vue SFC should use CSS modules when possible.
There are 2 special settings:
 - It is possible to use `._` as class name. It is replaced by the component name. It is useful for the root element.
 - It is possible to use camelCase in the template (`.myClass`) while using kebab-case on the CSS (`my-class`).

## Setup
```
yarn add @meaningfuldata/soil
```

### Dependencies
```
yarn add @fortawesome/vue-fontawesome @fortawesome/fontawesome-svg-core axios lodash-es vue-i18n
```

## Scripts

### Compile and serve the application for development
```
yarn serve
```

### Compile and minify for production
```
yarn build
```

### Run unit tests
```
yarn test:unit
```

#### Watch and run unit tests on changes
```
yarn test:unit:watch
```

#### Debug unit tests
```
yarn test:unit:inspect
```

### Run end-to-end tests
```
yarn test:e2e
```

#### Watch and run end-to-end tests on changes
```
yarn test:e2e:watch
```

### Lint and fix code
```
yarn lint
```

### Serve the storybook
```
yarn storybook
```

#### Build the storybook
```
yarn storybook:build
```

## License

Apache 2.0

## Authors
