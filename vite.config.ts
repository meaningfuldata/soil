import path from 'path'
import { defineConfig } from 'vite'
import type { OutputOptions } from 'rollup'
import vue from '@vitejs/plugin-vue'

const globals = {
  '@fortawesome/vue-fontawesome': 'VueFontAwesome',
  axios: 'axios',
  vue: 'Vue',
  'vue-i18n': 'VueI18n',
  'vue-router': 'VueRouter',
  'lodash-es': '_'
}

const output: OutputOptions = {
  inlineDynamicImports: true,
  exports: 'named',
  globals,
  assetFileNames: assetInfo => {
    if (assetInfo.name == 'style.css') {
      return 'soil.css'
    }
    return assetInfo.name
  }
}

export default defineConfig({
  plugins: [vue()],

  resolve: {
    alias: {
      '@.storybook': path.resolve(__dirname, './.storybook'),
      '@': path.resolve(__dirname, './src'),
      lodash: 'lodash-es'
    }
  },
  //This part doesn't work with storybook, comment it out and add it where needed in css to make it work
  css: {
    preprocessorOptions: {
      sass: {
        // Allow using the SASS variables into every Vue SFC component
        // NOTE: including CSS rules will be included (duplicated) on every SFC
        additionalData: `
          @use './src/styles/theme' as theme
        `
      }
    }
  },

  build: {
    lib: {
      name: 'Soil',
      entry: path.resolve(__dirname, './src/index.ts')
    },
    rollupOptions: {
      external: [
        '@fortawesome/fontawesome-svg-core',
        '@fortawesome/free-regular-svg-icons',
        '@fortawesome/free-solid-svg-icons',
        '@fortawesome/vue-fontawesome',
        'axios',
        'lodash-es',
        'joi',
        'vue',
        'vue-i18n',
        'vue-router'
      ],
      output: [
        {
          ...output,
          format: 'es',
          esModule: true
        },
        {
          ...output,
          format: 'umd',
          inlineDynamicImports: true,
          interop: 'esModule'
        }
      ]
    }
  }
})
