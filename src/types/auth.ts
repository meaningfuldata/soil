export type Username = string

export interface Credentials {
  username: Username
  password: string
}

export interface ChangePassword {
  newPassword: string
  oldPassword: string
}
export interface ResetPassword {
  newPassword: string
  userId?: string
  token?: string
}

export interface Email {
  email: string
}
