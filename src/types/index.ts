export * from './auth'
export * from './components'
export * from './dom'

export interface PluginOptions {
  prefix?: string
}
