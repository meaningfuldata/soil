// TODO: Create custom component, that wraps `RouterLink` and includes the title as property
export interface NamedRoute {
  name: string
  params?: Record<string, unknown>
}
