import i18n from './i18n/'
import * as routes from './routes/'
import * as services from './services/'
import './styles/index.sass'

export * from './config'
export * from './components/'
export * from './types/'
export * from './views/'
export * from './utils/'

export { i18n, routes, services }
