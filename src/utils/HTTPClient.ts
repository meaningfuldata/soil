import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig } from 'axios'

interface Config extends AxiosRequestConfig {
  baseURL?: string | (() => string)
  responseHandler?: any
}

/**
 * A very simple HTTP client.
 * It provides a thin layer around axios to avoid the usual boilerplate when implementing clients for different services.
 * For more complex use cases, axios is exposed as the `axios` property.
 */
export default class HTTPClient {
  public axios: AxiosInstance

  private handleResponse(response: AxiosResponse): any {
    return response
  }

  /**
   * This base URL will be evaluated on every request.
   *
   * It can be used to override the default `SERVICES.API.BASE_URL`.
   */
  private baseURL: null | (() => string) = null

  constructor(config: Config = {}) {
    if (config.baseURL instanceof Function) {
      this.baseURL = config.baseURL
    }
    delete config.baseURL

    if (config.responseHandler) {
      this.handleResponse = config.responseHandler
    }
    delete config.responseHandler

    this.axios = axios.create(config as AxiosRequestConfig)
  }

  async delete(url: string, config?: AxiosRequestConfig): Promise<any> {
    if (this.baseURL) {
      this.axios.defaults.baseURL = this.baseURL()
    }
    const response = await this.axios.delete(url, config)

    return this.handleResponse(response)
  }

  async get(url: string, config?: AxiosRequestConfig): Promise<any> {
    if (this.baseURL) {
      this.axios.defaults.baseURL = this.baseURL()
    }
    const response = await this.axios.get(url, config)

    return this.handleResponse(response)
  }

  async patch(
    url: string,
    data: any,
    config?: AxiosRequestConfig
  ): Promise<any> {
    if (this.baseURL) {
      this.axios.defaults.baseURL = this.baseURL()
    }
    const response = await this.axios.patch(url, data, config)

    return this.handleResponse(response)
  }

  async post(
    url: string,
    data: any,
    config?: AxiosRequestConfig
  ): Promise<any> {
    if (this.baseURL) {
      this.axios.defaults.baseURL = this.baseURL()
    }
    const response = await this.axios.post(url, data, config)

    return this.handleResponse(response)
  }
}
