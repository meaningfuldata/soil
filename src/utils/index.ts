import HTTPClient from './HTTPClient'

const logError = (message: string) => console.error(`[Soil] ${message}`)

import {
  downloadCSV,
  downloadFile,
  downloadImage,
  downloadJSON,
  readJsonFile
} from './download'

export {
  HTTPClient,
  downloadCSV,
  downloadFile,
  downloadImage,
  downloadJSON,
  readJsonFile,
  logError
}
