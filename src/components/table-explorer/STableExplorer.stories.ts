import { meta, template } from '@.storybook/utils'
import { STableExplorer } from '.'
import { action } from '@storybook/addon-actions'

export default meta({ STableExplorer })

const Template = args => ({
  components: {
    STableExplorer
  },
  setup() {
    return { args, items }
  },
  template: `<STableExplorer v-bind='args' @menu-input='handleMenuInput'/>`,
  methods: {
    handleMenuInput(evt: any) {
      action('menu-input')(evt)
    }
  }
})

const items = Array(20)
  .fill(
    {
      name: 'DUMMY',
      menu: [
        {
          title: 'Module options',
          items: [
            {
              title: 'Other example',
              icon: 'book'
            }
          ]
        }
      ],
      tables: [
        {
          name: 'datastructure',
          columns: [{ label: 'Text column', field: 'text', type: 'text' }],
          rows: [{ text: 'Value 1' }, { text: 'Value 2' }]
        },
        {
          name: 'datapoints',
          columns: [{ label: 'Text column', field: 'text', type: 'text' }],
          rows: [{ text: 'Value 1' }, { text: 'Value 2' }]
        }
      ]
    },
    0,
    20
  )
  .map((el: any, ind: number) => {
    return {
      ...el,
      name: el.name + ind
    }
  })

export const Example = Template.bind({})
Example.args = {
  items,
  label: 'Datasets',
  loading: false,
  mainMenu: [
    {
      title: 'Module options',
      items: [
        {
          title: 'Other example',
          icon: 'book'
        }
      ]
    }
  ]
}
