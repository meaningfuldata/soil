import { action } from '@storybook/addon-actions'
import { meta } from '@.storybook/utils'
import { SButton } from './'

export default meta({ SButton })

const Template = args => ({
  components: { SButton },
  setup() {
    return { args }
  },
  template: `<s-button v-bind="args" @buttonClick="buttonClick"/>`,
  methods: {
    buttonClick: action('buttonClicked')
  }
})

export const Simple = Template.bind({})
Simple.args = {
  label: 'Click',
  variant: null,
  size: null,
  expanded: false,
  disabled: false,
  outlined: false,
  rounded: false
}

Simple.argTypes = {
  size: {
    options: [null, 'small', 'medium', 'large'],
    control: 'select'
  },
  variant: {
    options: [null, 'primary', 'danger', 'info', 'warning'],
    control: 'select'
  }
}
