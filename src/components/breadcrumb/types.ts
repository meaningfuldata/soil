import { Icon } from '@/components/'
import { NamedRoute } from '@/types/'

export interface BreadcrumbItem {
  title: string
  icon?: Icon
  isActive?: boolean
  route?: NamedRoute
  data?: any
}
