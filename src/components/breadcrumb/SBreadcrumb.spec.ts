// import BIcon from 'buefy/src/components/icon/Icon.vue'
// import { BreadcrumbItem, SBreadcrumb } from './'
//
// const items: Array<BreadcrumbItem> = [
//   { title: 'Example 1', icon: 'battery-empty', route: { name: '' } },
//   { title: 'Example 2', icon: 'battery-empty', route: { name: '' } },
//   { title: 'Example 3', icon: 'battery-full', route: { name: '' } }
// ]
//
// describe('SBreadcrumb.vue', () => {
//   let wrapper: any
//
//   // This test can be used if props validators throw an error when fail
//
//   //   it('do not pass items prop', () => {
//   //     expect(() => {
//   //       localMount(SBreadcrumb, {
//   //         propsData: {}
//   //       })
//   //     }).to.throw('Breadcrumb can not be empty')
//   //   })
//
//   //   it('pass an empty items', () => {
//   //     expect(() => {
//   //       localMount(SBreadcrumb, {
//   //         propsData: { items: [] }
//   //       })
//   //     }).to.throw('Breadcrumb can not be empty')
//   //   })
//
//   it('pass one item', () => {
//     wrapper = localMount(SBreadcrumb, {
//       propsData: { items: [items[0]] }
//     })
//
//     expect(wrapper.findAll('li')).to.have.lengthOf(1)
//     expect(wrapper.findComponent(BIcon).props('icon')).to.be.eql(
//       'battery-empty'
//     )
//   })
//
//   it('pass multiples items', () => {
//     wrapper = localMount(SBreadcrumb, {
//       propsData: { items: items }
//     })
//
//     expect(wrapper.findAll('li')).to.have.lengthOf(3)
//     expect(wrapper.findAllComponents(BIcon).at(2).props('icon')).to.be.eql(
//       'battery-full'
//     )
//   })
// })
