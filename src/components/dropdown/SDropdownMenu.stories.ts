import { meta, template } from '@.storybook/utils'
import { SDropdownMenu, DropdownMenuItem, DropdownMenuList } from './'
import { action } from '@storybook/addon-actions'

export default meta({ SDropdownMenu })

const Template = args => ({
  components: { SDropdownMenu },
  setup() {
    return { args }
  },
  template: '<SDropdownMenu v-bind="args" @menu-input="menuClicked"/>',
  methods: {
    menuClicked: (val: string) => action('menu-clicked')(val)
  }
})

const items: Array<DropdownMenuItem> = [
  { title: 'Item 1', icon: 'home' },
  { title: 'Item 2', icon: 'flower' }
]

const lists: Array<DropdownMenuList> = [
  { title: 'List 1', icon: 'moon', items },
  {
    title: 'List 2',
    icon: 'sun',
    items: [{ title: 'Other item', icon: 'map' }]
  }
]

const largeList: Array<DropdownMenuItem> = [
  ...lists,
  ...lists,
  ...lists,
  ...lists
]

export const Empty = Template.bind({})
Empty.args = {
  title: 'Empty'
}

export const Items = Template.bind({})
Items.args = {
  title: 'Items',
  menu: items
}

export const Lists = Template.bind({})
Lists.args = {
  title: 'Lists',
  menu: lists
}

export const LargeLists = Template.bind({})
LargeLists.args = {
  title: 'Large List',
  menu: largeList
}
