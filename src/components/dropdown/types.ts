import { NamedRoute } from '@/types/'
import { Icon } from '@/components/'
import type { IconName } from '@/components/icon'

export interface DropdownMenuItem {
  title: string
  icon?: Icon | IconName
  route?: NamedRoute
  value?: string
  // The entry could be visible but disabled
  isDisabled?: boolean
  meta?: any
}

export interface DropdownMenuGroup {
  title: string
  icon?: Icon | IconName
  items: Array<DropdownMenuItem>
}

export interface DropdownMenuInputEvent {
  value: string
  object: any
}

export type DropdownMenu = Array<DropdownMenuGroup | DropdownMenuItem>
