import { meta, template } from '@.storybook/utils'
import { SSidebar, SidebarItem } from './'
import { SIcon, STree, SMenu } from '../'
import { treeExample } from '../tree/data'
import { menuExample } from '../menu/data'
import { action } from '@storybook/addon-actions'

export default meta({ SSidebar })

const Template = args => ({
  components: { SSidebar, SIcon, STree, SMenu },
  setup() {
    return { args, tree: treeExample, menu: menuExample }
  },
  template: `
  <div style="height: 80vh; width: 300px;">
  <SSidebar v-bind="args" @subbar-menu-input="subbarMenuClicked">
    <template v-if="${'sidebar-top' in args}" v-slot:sidebar-top>${
    args['sidebar-top']
  }</template>
    <template v-if="${'sidebar-body' in args}" v-slot:sidebar-body>${
    args['sidebar-body']
  }</template>
    <template v-if="${'sidebar-body-0' in args}" v-slot:sidebar-body-0>${
    args['sidebar-body-0']
  }</template>
    <template v-if="${'sidebar-body-1' in args}" v-slot:sidebar-body-1>${
    args['sidebar-body-1']
  }</template>
    <template v-if="${'sidebar-footer' in args}" v-slot:sidebar-footer>${
    args['sidebar-footer']
  }</template>
  </SSidebar>
  </div>
`,
  methods: {
    menuClicked: (val: string) => action('menu-clicked')(val),
    treeNodeClicked: (val: string) => action('tree-node-clicked')(val),
    subbarMenuClicked: (val: string) => action('subbar-menu-clicked')(val)
  }
})

export const SimpleSidebar = Template.bind({})
SimpleSidebar.args = {
  'sidebar-top': `<h1 style="font-size: 2em; text-align: center" class="p-3"><b>Soil</b>Sidebar</h1>`,
  'sidebar-body': `<s-tree class="m-2" :tree="tree" @menu-input="menuClicked" @tree-node-click="treeNodeClicked"/>`,
  'sidebar-footer': `<div style="opacity: 0.7; text-align: center" class="p-3">This is a footer 🦶</div>`
}

export const TreeSidebar = Template.bind({})
TreeSidebar.args = {
  subbar: [
    {
      icon: 'map',
      title: 'Explorer'
    },
    {
      icon: 'user',
      title: 'Options'
    }
  ],
  'sidebar-top': `<h1 style="font-size: 2em; text-align: center" class="p-3"><b>Soil</b>Sidebar</h1>`,
  'sidebar-body-0': `<s-tree class="m-2" :tree="tree" @menu-input="menuClicked" @tree-node-click="treeNodeClicked"/>`,
  'sidebar-body-1': `<s-menu :menu="menu"/>`,
  'sidebar-footer': `<div style="opacity: 0.7; text-align: center" class="p-3">This is a footer 🦶</div>`
}
