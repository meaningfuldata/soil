// import { RouterLinkStub as RouterLink } from '@vue/test-utils'
// import BIcon from 'buefy/src/components/icon/BIcon.vue'
// import { SidebarItem, SSidebar } from './'
//
// const items: Array<SidebarItem> = [
//   { title: 'Example 1', icon: 'home', route: { name: 'index' } },
//   { title: 'Example 2', icon: 'cog', route: { name: 'settings' } },
//   { title: 'Example 3', icon: 'user', route: { name: 'profile' } }
// ]
//
// describe('SSidebar.vue', () => {
//   let wrapper: any
//
//   beforeEach(() => {
//     wrapper = localMount(SSidebar, {
//       propsData: { items }
//     })
//   })
//
//   it('renders correctly', () => {
//     expect(wrapper.html()).toMatchSnapshot()
//   })
//
//   describe('when items have a `route` attribute', () => {
//     it('links to them on the icons', () => {
//       const links = wrapper.findAllComponents(RouterLink)
//
//       expect(links).to.have.length(3)
//
//       expect(links.at(0).props('to').name).to.eql('index')
//       expect(links.at(0).findComponent(BIcon).props('icon')).to.eql('home')
//
//       expect(links.at(1).props('to').name).to.eql('settings')
//       expect(links.at(1).findComponent(BIcon).props('icon')).to.eql('cog')
//
//       expect(links.at(2).props('to').name).to.eql('profile')
//       expect(links.at(2).findComponent(BIcon).props('icon')).to.eql('user')
//     })
//   })
//
//   describe('when items do not have a `route` attribute', () => {
//     it('displays the icons', () => {
//       const icons = wrapper.findAllComponents(BIcon)
//
//       expect(icons).to.have.length(3)
//       expect(icons.at(0).props('icon')).to.eql('home')
//       expect(icons.at(1).props('icon')).to.eql('cog')
//       expect(icons.at(2).props('icon')).to.eql('user')
//     })
//   })
// })
