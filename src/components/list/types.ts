import type { Icon, IconName } from '@/components/'

// TODO: allow optional unique id and fallback to label as key in v-for

interface Base {
  label: string
  icon?: Icon | IconName
  isCollapsible?: boolean
}

export interface List extends Base {
  items: ListItem[]
  isCollapsed: boolean
}

export interface ListItem extends Base {
  // Item content is collapsed by default
  isExpanded?: boolean
}
