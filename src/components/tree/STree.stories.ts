import { meta } from '@.storybook/utils'
import { action } from '@storybook/addon-actions'
import { STree, Tree } from './'
import { treeExample } from './data'
export default meta({ STree })

const Template = args => ({
  components: { STree },
  setup() {
    return { args }
  },
  template:
    '<STree v-bind="args" @menu-input="menuClicked" @tree-node-click="treeNodeClicked"/>',
  methods: {
    menuClicked: (val: string) => action('menu-clicked')(val),
    treeNodeClicked: (val: string) => action('tree-node-clicked')(val)
  }
})

export const RootOnlyTree = Template.bind({})
RootOnlyTree.args = {
  tree: { ...treeExample, children: [] }
}

export const NestedTrees = Template.bind({})
NestedTrees.args = {
  tree: treeExample
}

// TODO:
// export const MoreLevels = Template.bind({})
// MoreLevels.args = {
//   tree
// }
