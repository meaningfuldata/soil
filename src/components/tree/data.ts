import { Tree } from './types'

// Example tree
export const treeExample: Tree = {
  title: 'Transformation Schemas',
  textIcon: {
    text: 'TS'
  },
  hasCount: true,
  isClickable: true,
  menu: [
    {
      title: 'TS options',
      items: [
        {
          title: 'Example',
          icon: 'leaf'
        },
        {
          title: 'Example 2',
          icon: 'clock'
        }
      ]
    }
  ],
  children: [
    {
      title: 'Main',
      icon: 'battery-full',
      hasCount: true,
      children: [
        {
          title: 'Home',
          icon: 'leaf',
          isClickable: true,
          route: {
            name: 'home'
          },
          menu: [
            {
              title: 'TS options',
              items: [
                {
                  title: 'Example',
                  icon: 'leaf'
                },
                {
                  title: 'Example 2',
                  icon: 'clock'
                }
              ]
            }
          ],
          children: []
        },
        {
          title: 'Other',
          icon: 'leaf',
          isClickable: true,
          children: []
        }
      ]
    },
    {
      title: 'Secondary',
      icon: 'battery-full',
      isInitialCollapsed: true,
      hasCount: true,
      menu: [
        {
          title: 'Module options',
          items: [
            {
              title: 'Other example',
              icon: 'book'
            }
          ]
        }
      ],
      children: [
        {
          title: 'Other',
          icon: 'leaf',
          children: [],
          extraContent: {
            show: true,
            props: {
              icon: 'lock',
              style: {
                color: 'red',
                marginRight: '0.1em'
              }
            }
          }
        },
        {
          title: 'About',
          icon: 'leaf',
          children: []
        }
      ]
    }
  ]
}
