import { meta } from '@.storybook/utils'
import { action } from '@storybook/addon-actions'
import STreeExplorer from './STreeExplorer.vue'
import { SIcon } from '../icon/'
import { treeExample } from './data'

export default meta({ STreeExplorer })

const Template = args => ({
  components: { STreeExplorer, SIcon },
  setup() {
    return { args }
  },
  template: `<STreeExplorer v-bind="args" @menu-input="menuClicked" @tree-node-click="treeNodeClicked" @explorer-input="explorerInput">
      <template v-if="${
        'empty-search-results' in args
      }" v-slot:empty-search-results>${args['empty-search-results']}</template>
      <template v-slot:extraContent='extraContent'>
        <s-icon :icon="extraContent.icon" :iconStyle="extraContent.style" />
      </template>

    </STreeExplorer>
    `,
  methods: {
    menuClicked: (val: string) => action('menu-clicked')(val),
    treeNodeClicked: (val: string) => action('tree-node-clicked')(val),
    explorerInput: (val: string) => action('explorer-input')(val)
  }
})

export const Example = Template.bind({})
Example.args = {
  trees: [
    treeExample,
    {
      ...treeExample,
      title: 'Library Items',
      textIcon: { text: 'LI', iconStyle: { verticalAlign: 'middle' } }
    }
  ]
}

export const CustomEmptySearch = Template.bind({})

CustomEmptySearch.args = {
  trees: [
    treeExample,
    {
      ...treeExample,
      title: 'Library Items',
      textIcon: { text: 'LI', iconStyle: { verticalAlign: 'middle' } }
    }
  ],
  'empty-search-results':
    '<p style="text-align: center">This is an example of an empty search slot</p>'
}
