import type { DropdownMenu } from '@/components/dropdown/types'
import { NamedRoute } from '@/types/'
import type { TextIcon, Icon, IconName } from '@/components/icon'

interface TreeOption {
  name: string
  icon?: Icon
}
export interface TreeMenu {
  title: string
  icon?: Icon
  entries?: Array<TreeOption>
}

interface TreeExtraContent {
  show: boolean
  props: any
}

// TODO: allow functions to obtain values
export interface Tree {
  title: string
  icon?: Icon | IconName
  textIcon?: TextIcon
  description?: string
  isInitialCollapsed?: boolean
  extraContent?: TreeExtraContent
  isClickable?: boolean
  hasCount?: boolean
  route?: NamedRoute
  menu?: DropdownMenu
  children?: Array<Tree>
  meta?: any
}
