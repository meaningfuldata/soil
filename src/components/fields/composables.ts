import { computed } from 'vue'
import { useI18n } from 'vue-i18n'

export function useFieldProps(message: string) {
  return {
    type: 'default',
    modelValue: '',
    label() {
      const { t } = useI18n()
      return t(message)
    }
  }
}

export function useHasCounter(type: any) {
  return {
    hasCounter: computed((): boolean => type.value !== 'default')
  }
}
