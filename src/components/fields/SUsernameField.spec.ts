// import { SUsernameField } from './'
// import BInput from 'buefy/src/components/input/Input.vue'
// import BField from 'buefy/src/components/field/Field.vue'
//
// describe('SUsernameField.vue', () => {
//   let wrapper: any
//
//   beforeEach(() => {
//     wrapper = localMount(SUsernameField, {
//       propsData: { value: '' }
//     })
//   })
//
//   it('renders correctly', () => {
//     expect(wrapper.html()).toMatchSnapshot()
//   })
//
//   it('without props', () => {
//     expect(wrapper.findComponent(BField).props('label')).to.be.eql('Username')
//   })
//
//   it('passing a label', async () => {
//     await wrapper.setProps({ label: 'New Username' })
//     expect(wrapper.findComponent(BField).props('label')).to.be.eql(
//       'New Username'
//     )
//   })
//
//   it('has not counter', () => {
//     expect(wrapper.findComponent(BInput).props('hasCounter')).to.be.false
//   })
// })
