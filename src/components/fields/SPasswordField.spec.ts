// import { SPasswordField } from './'
// import BInput from 'buefy/src/components/input/Input.vue'
// import BField from 'buefy/src/components/field/Field.vue'
//
// describe('SPasswordField.vue', () => {
//   let wrapper: any
//
//   beforeEach(() => {
//     wrapper = localMount(SPasswordField, {
//       propsData: { value: '' }
//     })
//   })
//
//   it('renders correctly', () => {
//     expect(wrapper.html()).toMatchSnapshot()
//   })
//
//   it('without props', () => {
//     expect(wrapper.findComponent(BField).props('label')).to.be.eql('Password')
//   })
//
//   it('passing a label', async () => {
//     await wrapper.setProps({ label: 'Repeat Password' })
//     expect(wrapper.findComponent(BField).props('label')).to.be.eql(
//       'Repeat Password'
//     )
//   })
//
//   it('has not counter', () => {
//     expect(wrapper.findComponent(BInput).props('hasCounter')).to.be.false
//   })
// })
