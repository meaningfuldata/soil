import { meta, template } from '@.storybook/utils'
import { STextIcon } from './'

export default meta({ STextIcon })

const Template = template({ STextIcon })

export const DefaultStyle = Template.bind({})
DefaultStyle.args = {
  text: 'mdl'
}

export const CustomStyle = Template.bind({})
CustomStyle.args = {
  text: 'mdl',
  iconStyle: {
    fontSize: '1.5em',
    fontStyle: 'italic',
    fontWeight: 'normal',
    color: 'red'
  }
}
