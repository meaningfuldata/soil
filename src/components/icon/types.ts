// TODO: restrict the available icons
export type IconName = string
export type IconPack = 'fab' | 'fad' | 'fal' | 'far' | 'fas'

export interface Icon {
  name: IconName
  pack: IconPack
  // The `title` HTML attribute
  title?: string
  isCircled?: boolean
}

export interface TextIcon {
  style?: CSSStyleDeclaration
  text: string
}
