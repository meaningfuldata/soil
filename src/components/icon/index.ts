import SIcon from './SIcon.vue'
import STextIcon from './STextIcon.vue'

export { SIcon, STextIcon }
export * from './types'
