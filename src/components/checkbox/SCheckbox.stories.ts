import { meta, template } from '@.storybook/utils'
import { SCheckbox } from './'

export default meta({ SCheckbox })

const Template = template({ SCheckbox })

export const Simple = Template.bind({})
Simple.args = {
  variant: 'danger',
  text: 'Checkbox'
}
Simple.argTypes = {
  variant: {
    options: [null, 'success', 'danger', 'info', 'warning'],
    control: 'select'
  }
}
