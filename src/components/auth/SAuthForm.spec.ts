import { SAuthForm } from './'

describe('SAuthForm.vue', () => {
  let wrapper: any

  beforeEach(() => {
    wrapper = localMount(SAuthForm, {
      propsData: { title: 'Example' }
    })
  })

  it('renders correctly', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
