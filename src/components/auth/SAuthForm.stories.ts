import { Story } from '@storybook/vue/types-6-0'
import { meta, template } from '@.storybook/utils'
import { SAuthForm } from './'

export default meta({ SAuthForm })

const EmptyTemplate = template({ SAuthForm })

const Template: Story = (args: Record<string, unknown>) => ({
  components: { SAuthForm },
  setup() {
    return { args }
  },
  template: `
    <s-auth-form v-bind="args">
      <template #left>
        <p>Left slot content</p>
      </template>
      <template #right>
        <p>Right slot content</p>
      </template>
    </s-auth-form>`
})

export const Empty = EmptyTemplate.bind({})
Empty.args = {
  title: 'Title (without slots)'
}

export const Slot = Template.bind({})
Slot.args = {
  title: 'Title (with slots)'
}
