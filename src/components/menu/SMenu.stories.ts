import { meta, template } from '@.storybook/utils'
import { action } from '@storybook/addon-actions'
import { omit } from 'lodash'
import { SMenu } from './'
import { menuExample } from './data'

export default meta({ SMenu })

const Template = args => ({
  components: { SMenu },
  setup() {
    return { args }
  },
  template: `<s-menu v-bind="args" @menu-input="menuClicked" />`,
  methods: {
    menuClicked: (val: string) => action('menu-clicked')(val)
  }
})

export const Simple = Template.bind({})

Simple.args = {
  menu: menuExample.map(e =>
    omit(e, ['disabled', 'upperLeft', 'upperRight', 'downLeft', 'downRight'])
  )
}

export const ExtraInformation = Template.bind({})

ExtraInformation.args = {
  menu: menuExample.map(e => omit(e, ['disabled']))
}

export const NotSelectable = Template.bind({})

NotSelectable.args = {
  menu: menuExample,
  mantainSelection: false
}
