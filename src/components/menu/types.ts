import type { DropdownMenuItem } from '@/components/'

export interface ContextMenuEntry<Value> {
  label: string
  value: Value
  isDisabled?: boolean
}

export interface MenuItem {
  // TODO: rename to label
  title: string
  // TODO: rename to isActive
  active: boolean
  // TODO: rename to isDisabled
  disabled: boolean
}

export interface MenuBarItem {
  // TODO: rename to label
  title: string
  // TODO: rename to entries
  menu?: DropdownMenuItem[]
}
