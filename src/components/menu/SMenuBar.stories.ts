import { meta, template } from '@.storybook/utils'
import { SMenuBar } from '..'
import type { MenuBarItem } from './types'

export default meta({ SMenuBar })

const Template = template({ SMenuBar })

const menuOptions: MenuBarItem[] = [
  {
    title: 'Home',
    menu: [{ title: 'h1', route: 'home' }, { title: 'h2' }]
  },
  {
    title: 'News',
    menu: [
      { title: 'n1' },
      { title: 'n2' },
      { title: 'n3', route: { name: 'special-news' } }
    ]
  },
  {
    title: 'Contact',
    menu: [{ title: 'c1' }, { title: 'c2' }, { title: 'c3' }]
  }
]

export const MenuBar = Template.bind({})
MenuBar.args = {
  menu: menuOptions
}
