import { mount } from '@vue/test-utils'
import type { VueWrapper } from '@vue/test-utils'
import SMenuBar from './SMenuBar.vue'

const wrap = (props: any) => {
  return mount(SMenuBar, {
    props
  })
}

describe('SMenuBar.vue', () => {
  let wrapper: VueWrapper<any>
})
