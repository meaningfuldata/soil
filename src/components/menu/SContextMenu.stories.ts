import { meta, template } from '@.storybook/utils'
import { SContextMenu } from './'
import { contextMenuExample } from './data'

export default meta({ SContextMenu })

const Template = template({ SContextMenu })
const CustomTemplate = template(
  { SContextMenu },
  {
    template: `<s-context-menu v-bind="args">
    <template v-slot='entry'>
      Custom slot for <b>{{ entry.label }}</b>
    </template>
  </s-context-menu>`
  }
)

export const Default = Template.bind({})

Default.args = {
  entries: contextMenuExample
}

export const CustomEntry = CustomTemplate.bind({})

CustomEntry.args = {
  entries: contextMenuExample
}
