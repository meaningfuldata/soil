import type { Component, Props } from 'vue'

interface NotificationConfig {
  oruga: any
  message?: string
  component?: Component
  props?: Props
  // Oruga does not provide the types
  [x]: any
}

// TODO: Oruga requires being installed as plugin to use `useProgrammatic`, so,
// until deciding a better approach, `oruga` instance is passed as configuration
const openNotification = (config: NotificationConfig) => {
  const { oruga } = config
  delete config.oruga

  const defaultConfig = {
    rootClass: 'soil-notification',
    variantClass: 'soil-notification--',
    autoClose: false,
    position: 'bottom',
    closable: true,
    indefinite: true
  }

  return oruga.notification.open({ ...defaultConfig, ...config })
}

export { openNotification }
