import { meta, template } from '@.storybook/utils'
import { STable, Column, Row } from './'

export default meta({ STable })

const Template = template({ STable })
const CustomTemplate = template(
  { STable },
  {
    template: `<s-table v-bind="args">
    <template #textA="{ row }">
      <b>{{ row.textA }}</b>
    </template>
    <template #textB="{ row }">
      <i>{{ row.textB }}</i>
    </template>
  </s-table>`
  }
)

const columns = [
  { label: 'Column A', field: 'textA', type: 'text' },
  { label: 'Column B', field: 'textB', type: 'text' }
] as Column[]

const rows = [
  { textA: 'Value A1', textB: 'Value B1' },
  { textA: 'Value A2', textB: 'Value B2' }
] as Row[]

export const EmptyRows = Template.bind({})
EmptyRows.args = {
  columns,
  rows: []
}

export const TextColumns = Template.bind({})
TextColumns.args = {
  columns,
  rows
}

export const CustomRow = CustomTemplate.bind({})
CustomRow.args = {
  columns: columns.map(({ label, field }) => ({ field, label })),
  rows
}
