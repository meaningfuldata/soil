export interface ColumnSelectFieldOption {
  id: string
  value: string
}

export interface ColumnSelectField {
  placeholder?: string
  options: Array<string | ColumnSelectFieldOption>
}

export interface Column {
  field: string
  label?: string
  placeholder?: string
  selectField?: ColumnSelectField
  tooltip?: string
  sortable?: boolean
  searchable?: boolean
  type?: string
}

export type Row = Record<string, any>
