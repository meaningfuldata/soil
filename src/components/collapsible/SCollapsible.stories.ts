import { meta, template } from '@.storybook/utils'
import { SCollapsible } from '.'
import { action } from '@storybook/addon-actions'

export default meta({ SCollapsible })

const NoTriggerTemplate = template(
  { SCollapsible },
  {
    template: `<s-collapsible v-bind="args">
    <template #trigger='{ isCollapsed }'>
      Trigger for <b>{{ isCollapsed ? "collapsed" : "expanded" }}</b> content
    </template>

    The content
  </s-collapsible>`
  }
)
const TriggerTemplate = template(
  { SCollapsible },
  {
    template: `<s-collapsible v-bind="args">
    <template #trigger='{ isCollapsed }'>
      Trigger for <b>{{ isCollapsed ? "collapsed" : "expanded" }}</b> content
    </template>

    The content
  </s-collapsible>`
  }
)

export const NoTrigger = NoTriggerTemplate.bind({})
NoTrigger.args = {
  isCollapsed: false
}

export const ClickTrigger = TriggerTemplate.bind({})
ClickTrigger.args = {
  isCollapsed: true,
  toggleEvent: 'click'
}

export const MouseEnterLeaveTrigger = TriggerTemplate.bind({})
MouseEnterLeaveTrigger.args = {
  isCollapsed: true,
  collapseEvent: 'mouseleave',
  expandEvent: 'mouseenter'
}
