import { Schema } from 'joi'

export interface FormInput {
  name: string
  label: string
  type: string
  value: any
  state?: string
  msg?: string
  text?: string
  validator?: Schema
  selectValues?: Array<string>
  options: any
}

export interface Form {
  inputForms: Array<FormInput>
}
