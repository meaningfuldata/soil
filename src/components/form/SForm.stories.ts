import { meta, template } from '@.storybook/utils'
import { SForm } from './'
import { OButton } from '@oruga-ui/oruga-next/src/components/button'
import Joi from 'joi'
import { action } from '@storybook/addon-actions'

export default meta({ SForm })

const Template = args => ({
  components: { SForm },
  setup() {
    return { args }
  },
  template: `
    <o-button @click='$refs.modal_form.open()'>Open form</o-button>
    <SForm v-bind="args" @submit="submit" ref="modal_form"/>
  `,
  methods: {
    submit: action('submit')
  }
})

export const SomeFields = Template.bind({})
SomeFields.args = {
  fields: [
    {
      label: 'Input',
      value: '',
      state: '',
      msg: '',
      type: 'input',
      validator: Joi.string().alphanum().min(5).max(30).required()
    },
    {
      label: 'Checkbox',
      value: false,
      type: 'checkbox',
      options: {
        text: 'This is a checkbox'
      }
    },
    {
      label: 'Textarea',
      value: '',
      state: '',
      msg: '',
      type: 'input',
      validator: Joi.string().min(10).max(100).required(),
      options: {
        expanded: true,
        type: 'textarea'
      }
    }
  ],
  isInitialActive: true
}

export const OtherFields = Template.bind({})
OtherFields.args = {
  fields: [
    {
      label: 'Select',
      value: null,
      state: '',
      type: 'select',
      validator: Joi.string().required(),
      selectValues: ['Option 1', 'Option 2', 'Option 3'],
      options: {
        placeholder: 'select'
      }
    },
    {
      label: 'Date',
      value: null,
      state: '',
      msg: '',
      validator: Joi.date().required(),
      type: 'date',
      options: {
        locale: 'es',
        placeholder: 'Click to select...',
        firstDayOfWeek: 1
      }
    },
    {
      label: 'Switch',
      value: false,
      type: 'switch'
    },
    {
      label: 'File',
      value: null,
      state: '',
      msg: '',
      type: 'file',
      validator: Joi.object({
        type: Joi.string().valid('application/json'),
        size: Joi.number().max(300).required()
      }),
      options: {
        expanded: true
        //multiple: true next to implement
      }
    }
  ],
  isInitialActive: true
}
