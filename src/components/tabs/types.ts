import { NamedRoute } from '@/types/'
import { Icon } from '@/components/'

export type TabId = number | string

export interface Tab<T> {
  id: TabId
  title: string
  icon?: Icon
  isLoading?: boolean
  // NOTE: not used yet
  isPinned?: boolean
  route?: NamedRoute
  // TODO: remove?
  content: T
}

export type TabListPosition = 'right'
