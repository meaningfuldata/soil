import { Story } from '@storybook/vue/types-6-0'
import { meta, template } from '@.storybook/utils'
import { STabs, Tab } from './'

export default meta({ STabs })

const Template = template({ STabs })

type StringTab = Tab<string>

const generateTabs = (number: number): Array<StringTab> => {
  const tabs: Array<StringTab> = []
  for (let i = 1; i <= number; i++) {
    tabs.push({
      id: i,
      title: `Tab ${String.fromCharCode(65 + i)}`,
      content: `Content ${String.fromCharCode(65 + i)}`
    })
  }
  return tabs
}

export const Empty = Template.bind({})
Empty.args = {
  tabs: []
}

export const OneTab = Template.bind({})
OneTab.args = {
  tabs: generateTabs(1)
}

export const SomeTabs = Template.bind({})
SomeTabs.args = {
  tabs: generateTabs(5)
}

export const TooMuchTabs = Template.bind({})
TooMuchTabs.args = {
  tabs: generateTabs(20)
}

export const LinkedTabs = Template.bind({})
LinkedTabs.args = {
  tabs: generateTabs(5).map((tab: StringTab) => {
    return {
      ...tab,
      route: { name: 'tab', params: { id: tab.id } }
    }
  })
}

const ContentTemplate: Story = (args: Record<string, unknown>) => ({
  components: { STabs },
  setup() {
    return { args }
  },
  template: `
  <s-tabs v-bind='args'>
    <template #content='{ tab }'>
      Tab: {{ tab.content }}
    </template>
  </s-tabs>`
})

export const TabsWithContent = ContentTemplate.bind({})
TabsWithContent.args = {
  tabs: generateTabs(4)
}
