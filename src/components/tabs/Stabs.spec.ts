// import STab from './STab.vue'
// import STabList from './STabList.vue'
// import { STabs } from './'
// import { Tab } from './types'
//
// const tabs: Array<Tab> = [
//   {
//     id: 1,
//     title: 'Tab 1',
//     icon: 'user',
//     isLoading: false
//   },
//   {
//     id: 2,
//     title: 'Tab 2',
//     icon: 'user',
//     isLoading: false
//   },
//   {
//     id: 3,
//     title: 'Tab 3',
//     icon: 'user',
//     isLoading: false
//   }
// ]
//
// describe('STabs.vue', () => {
//   let wrapper: any
//
//   beforeEach(() => {
//     wrapper = localMount(STabs, {
//       propsData: { tabs: [] }
//     })
//   })
//
//   it('renders correctly', () => {
//     expect(wrapper.html()).toMatchSnapshot()
//   })
//
//   it('empty tabs', () => {
//     expect(wrapper.findComponent(STab).exists()).to.be.false
//     expect(wrapper.findComponent(STabList).exists()).to.be.true
//   })
//
//   it('one tab', async () => {
//     await wrapper.setProps({ tabs: [tabs[0]] })
//     expect(wrapper.findAllComponents(STab)).to.have.lengthOf(1)
//     expect(wrapper.findComponent(STabList).exists()).to.be.true
//   })
//
//   it('several tabs', async () => {
//     await wrapper.setProps({ tabs: tabs })
//     expect(wrapper.findAllComponents(STab)).to.have.lengthOf(3)
//     expect(wrapper.findComponent(STabList).exists()).to.be.true
//   })
//
//   describe('with all tabs', () => {
//     let items: any
//     beforeEach(() => {
//       wrapper = localMount(STabs, {
//         propsData: { tabs: tabs }
//       })
//
//       items = wrapper.findAllComponents(STab)
//     })
//
//     it('is close to left disabled', () => {
//       expect(items.at(0).props('isCloseToLeftDisabled')).to.be.true
//       expect(items.at(1).props('isCloseToLeftDisabled')).to.be.false
//       expect(items.at(2).props('isCloseToLeftDisabled')).to.be.false
//     })
//
//     it('is close to right disabled', () => {
//       expect(items.at(0).props('isCloseToRightDisabled')).to.be.false
//       expect(items.at(1).props('isCloseToRightDisabled')).to.be.false
//       expect(items.at(2).props('isCloseToRightDisabled')).to.be.true
//     })
//
//     it('emited switch event when SwitchTab is called', async () => {
//       wrapper.vm.switchTab(tabs[0])
//
//       await wrapper.vm.$nextTick()
//
//       const valueEmitted = wrapper.emitted().switch
//
//       expect(valueEmitted).to.be.ok
//       expect(valueEmitted[0][0]).to.be.eql(tabs[0])
//     })
//
//     it('index of a tab', async () => {
//       await wrapper.setProps({ activeTabId: 1 })
//
//       expect(wrapper.vm.index).to.be.eql(1)
//     })
//   })
// })
