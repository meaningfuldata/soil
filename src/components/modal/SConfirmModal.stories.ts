import { meta, template } from '@.storybook/utils'
import { action } from '@storybook/addon-actions'
import { SConfirmModal } from './'
import { OButton } from '@oruga-ui/oruga-next/src/components/button'

export default meta({ SConfirmModal })

const Template = args => ({
  components: { SConfirmModal },
  setup() {
    return { args }
  },
  template: `
    <OButton @click='$refs.custom_form.open()'>Open modal</OButton>
    <SConfirmModal v-bind="args" @confirm="confirm" ref="custom_form"/>
    `,
  methods: {
    confirm: action('confirm')
  }
})

export const InfoModal = Template.bind({})
InfoModal.args = {
  isInitialActive: true,
  title: 'Success',
  content: 'You has been delete this model correctly',
  closeButtonText: 'Close'
}

export const ConfirmModalButtons = Template.bind({})
ConfirmModalButtons.args = {
  isInitialActive: true,
  title: 'Are you sure?',
  content:
    'You are trying to create a new version with uncommited changes, are you sure that, this can produce errors on results',
  closeButtonText: 'Cancel',
  actionButtonText: 'Confirm'
}

export const ConfirmModalTyping = Template.bind({})
ConfirmModalTyping.args = {
  isInitialActive: true,
  title: 'Delete Module',
  content: 'To delete module Ext2r please type "delete"',
  closeButtonText: 'Cancel',
  actionButtonText: 'Delete',
  actionButtonVariant: 'danger',
  inputPlaceholder: 'Type...'
}
