import { getConfig } from '@/config'
import { HTTPClient } from '@/utils/'

// TODO: check all endpoints
// TODO: use `payload` instead of `results`?
export interface ApiResponseData {
  results: Array<any>
  errors?: Array<any>
  warnings?: Array<any>
}

export interface ApiResponse {
  data: ApiResponseData
}

const clientConfig = {
  withCredentials: true,
  xsrfCookieName: 'csrftoken',
  xsrfHeaderName: 'X-CSRFToken',
  baseURL: () => getConfig('SERVICES.API.BASE_URL'),
  // Log errors, but always return the results
  responseHandler: (response: ApiResponse) => {
    const { data } = response

    // TODO: decide how to manage errors
    if (data.errors && data.errors.length) {
      console.error(data.errors)
    }

    return data.results || data
  }
}

export default new HTTPClient(clientConfig)
