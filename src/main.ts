import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
// TODO: optimize styles
import Oruga from '@oruga-ui/oruga-next'
// TODO: configurable
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

import App from './App.vue'
import i18n from '@/i18n/'
import './styles/index.sass'
import { auth, error } from './routes'
library.add(fas)
dom.watch()

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'root',
      component: () => import('@/App.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/App.vue')
    },
    {
      path: '/settings',
      name: 'settings',
      component: () => import('@/App.vue')
    },
    {
      path: '/tabs/:id',
      name: 'tab',
      component: () => import('@/App.vue')
    },
    ...auth,
    ...error
  ]
})

createApp(App).use(router).use(i18n).use(Oruga).mount('#app')
