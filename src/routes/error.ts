import { RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/:any',
    component: () => import('@/layouts/Default/_.vue'),
    children: [
      {
        path: '',
        name: '404',
        component: () => import('@/views/error/404.vue')
      }
    ]
  }
]

export default routes
