import { RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/auth/',
    component: () => import('@/layouts/Default/_.vue'),
    children: [
      {
        path: 'sign-in',
        name: 'sign-in',
        component: () => import('@/views/auth/SignIn.vue')
      },
      {
        path: 'sign-out',
        name: 'sign-out',
        component: () => import('@/views/auth/SignOut.vue')
      },
      {
        path: 'password/reset',
        name: 'password-reset',
        component: () => import('@/views/auth/PasswordReset.vue')
      },
      {
        path: 'password/change',
        name: 'password-change',
        component: () => import('@/views/auth/PasswordChange.vue')
      },
      {
        path: 'password/change/reset',
        name: 'password-change-reset',
        component: () => import('@/views/auth/PasswordChangeReset.vue')
      }
    ]
  }
]

export default routes
