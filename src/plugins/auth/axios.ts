// Executes the onUnathorized callback if a 401 status code is
// received through the client
const addServiceInterceptor = (
  service: any,
  onUnathorized: () => any
): void => {
  service.interceptors.response.use(
    (config: any) => {
      return config
    },
    (error: any) => {
      if (error.response.status === 401) onUnathorized()
      return Promise.reject(error)
    }
  )
}
