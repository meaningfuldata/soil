import { app } from "@storybook/vue3";
import { createRouter, createWebHistory } from 'vue-router'

import { library } from '@fortawesome/fontawesome-svg-core'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import i18n from '@/i18n/'
import Oruga from '@oruga-ui/oruga-next'

import '../src/styles/index.sass'

library.add(far)
library.add(fas)

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'root',
      component: () => import('@/App.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/App.vue')
    },
    {
      path: '/settings',
      name: 'settings',
      component: () => import('@/App.vue')
    },
    {
      path: '/tabs/:id',
      name: 'tab',
      component: () => import('@/App.vue')
    }
  ]
})

app.use(router)
app.use(i18n)
app.component('font-awesome-icon', FontAwesomeIcon)
app.use(Oruga)
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
