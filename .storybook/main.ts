const path = require('path');

module.exports = {
  stories: ['../src/**/*.stories.ts'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials', '@storybook/addon-actions'],
  core: {
    builder: '@storybook/builder-vite'
  },

  async viteFinal(config, {
    configType
  }) {
    config.resolve.alias['@'] = path.resolve(__dirname, '../src');
    config.resolve.alias['@.storybook'] = path.resolve(__dirname, './');
    return config;
  }

};